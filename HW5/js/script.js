document.addEventListener('DOMContentLoaded', async () => {
    let characters = [];
    let films = [];
    let character_request = [];
    let films_req = await serverRequest('https://swapi.dev/api/films/'); // request to achieve films list
    films_req.results.forEach(element => {
        let film_id = films.length;
        films[film_id] = {
            name: element.title,
            episode: element.episode_id,
            about: element.opening_crawl,
            heroes: []
        }
        element.characters.forEach(character => {
            let character_id = character.split('/')[character.split('/').length-2];
            character_request.push(character_id);
            films[film_id].heroes.push(character_id);
        });
    });
    character_request = [...new Set(character_request)];
    character_request = character_request.sort();
    for (let index = 0; index < character_request.length; index++) {
        const element = character_request[index];
        let persons = await serverRequest(`https://swapi.dev/api/people/${element}`); // request to achieve heroes list
        console.log(persons.name, index);
        characters[element] = persons.name;
    }
    films.forEach(element => {
        let container = document.createElement('div');
        container.classList.add('episode-card');

        let name = document.createElement('p');
        name.classList.add('episode-desc');
        name.innerText = `Name: ${element.name}`;

        let episode = document.createElement('p');
        episode.classList.add('episode-desc');
        episode.innerText = `Episode: ${element.episode}`;

        let about = document.createElement('p');
        about.classList.add('episode-desc');
        about.innerText = `Synopsis: ${element.about}`;

        let character_container = document.createElement('div');
        character_container.classList.add('episode-desc');

        let character_label = document.createElement('p');
        character_label.classList.add('episode-desc');
        character_label.innerText = 'Heroes: ';

        let character_ul_container = document.createElement('ul');
        character_label.classList.add('episode-desc');

        element.heroes.forEach(element => {
            let character_li = document.createElement('li');
            character_li.classList.add('episode-desc');
            character_li.innerText = characters[element];
            character_ul_container.appendChild(character_li);
        });

        character_container.appendChild(character_label);
        character_container.appendChild(character_ul_container);

        container.appendChild(name);
        container.appendChild(episode);
        container.appendChild(about);
        container.appendChild(character_container);
        document.querySelector('.content').appendChild(container);
    });
})