document.addEventListener('DOMContentLoaded', function() {
    const content = document.querySelector('#content');
    content.classList.add('active');
    getFilms();
});

async function getFilms() {
    const response = await fetch(`https://swapi.dev/api/films/`);
    const result = await response.json();
    let title = result.results;
    console.log(title);
    let ul = document.createElement('ul');
    let content = document.getElementById('content');
    content.appendChild(ul);
    title.map(function(elem) {
        let li = document.createElement('li');
        let btn = document.createElement('button');
        btn.innerHTML = 'Load characters';
        btn.setAttribute('class', 'btn');
        li.innerHTML = `${elem.episode_id}` + '<br/>' + `${elem.title}` + '<br/>' + `${elem.opening_crawl}` + '<br/>';
        ul.appendChild(li);
        li.appendChild(btn);
        btn.addEventListener('click', function() {
            btn.classList.add('active');
            const charactersNames = [];
            const characters = elem.characters.map(character => fetch(character));
            Promise.all(characters)
                .then(res => Promise.all(res.map(item => item.json())))
                .then(res => {
                    res.forEach(char => charactersNames.push(char.name));
                    let div = document.createElement('div');
                    li.appendChild(div);
                    div.innerHTML = charactersNames.join(', ');
                })
        })

    });
}