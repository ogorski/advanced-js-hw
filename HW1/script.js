/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/

function Hamburger(size, stuffing) {
    if(arguments.length !== 2) {
        throw newHamburgerException('You chosed wrong number of size and stuffing. Please choose size and stuffing correctly.');
    } else if (!Hamburger.TYPE_SIZE.includes(size)) {
        throw newHamburgerException('You chosed wrong size. Please choose big hamburger size or small hamburger size.');
    } else if (!Hamburger.TYPE_STUFFING.includes(stuffing)) {
        throw newHamburgerException('You chosed wrong stuffing. Please choose cheese or salad next time.');
    }

    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {size: "small", price: "10", calories: "45" }
Hamburger.SIZE_LARGE = {size: "large", price: "25", calories: "100" }
Hamburger.STUFFING_CHEESE = {stuffing: "cheese", price: "5", calories: "2"}
Hamburger.STUFFING_SALAD = {stuffing: "salad", price: "10", calories: "50"}
Hamburger.STUFFING_POTATO = {stuffing: "potato", price: "25", calories: "60"}
Hamburger.TOPPING_MAYO = {topping: "mayo", price: "5", calories: "40"}
Hamburger.TOPPING_SPICE = {topping: "spice", price: "10", calories: "10"}

Hamburger.TYPE_SIZE = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
Hamburger.TYPE_STUFFING = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD];
Hamburger.TYPE_TOPPING = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (addedTopping) {
    if (arguments.length !== 1) {
        throw newHamburgerException('You chosed wrong topping arguments. Please choose mayo or spice next time.');
    }
    if (!Hamburger.TYPE_TOPPING.includes(addedTopping)) {
        throw newHamburgerException('You chosed wrong topping for hamburger. It have to be mayo or spice.');
    }
    this.topping.forEach(item => {
        if (item === itemTopping) {
            throw newHamburgerException('You added topping: "${addTopping.topping}" before. Please add another topping.');
        }
    }); 
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (removedTopping) {
    if (arguments.length == !1) {
        throw newHamburgerException('You`ve entered wrong numbers of arguments.');
    } else if (!Hamburger.TYPE_TOPPING.includes(removedTopping)) {
        throw newHamburgerException('You`ve chosed wrong type of topping. Please choose mayo or spice.');
    } else if (this.topping.length === 0) {
        throw newHamburgerException('The hamburger topping is empty. Please add some topping before you remove it.');
    }

    if (this.topping.indexOf(removedTopping) !== -1) {
        this.topping.splice(this.topping.indexOf(removedTopping), 1);
    } else {
        throw newHamburgerException('You`ve tried to remove topping which did`nt added in hamburger. Please check your request.');
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.topping.map(el => el.topping);
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing.stuffing;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let priceTopping = this.topping.reduce((sum, current) => {
        return sum + current.calories;
    }, 0);
    return this.size.price + this.stuffing.price + priceTopping;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let caloriesTopping = this.topping.reduce((sum, current) => {
        return sum + current.calories;
    }, 0);
    return this.size.calories + this.stuffing.calories + caloriesTopping;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException () {
   this.name = "Hambuger Exception";
   this.message = message;
} 
HamburgerException.prototype = Error.prototype;

try {
    // берем маленький гамбургер с салатом
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
    // добавляем начинку из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // рассчитываем кол-во калорий в данном гамбургере
    console.log("Calories: %f", hamburger.calculateCalories());
    // рассчитываем цены данного гамбургера
    console.log("Price: %f", hamburger.calculatePrice());
    // добавляем начинку в виде специй
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // рассчитываем цену гамбургера с соусом
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // рассчитываем большой ли гамбургер
    console.log("Is this hamburger big: %f", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // убираем у гамбургера начинку из майонеза
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
} catch (e) {
    console.log(e);
}

try {
    /* мы не передали здесь обязательные значения
    let h2 = new Hamburger(); => HamburgerException: no size given 

    передаем заведомо неправильные значения
    let h3 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_MAYO);
    => HamburgerException: invalid size: "TOPPING_SAUCE"

    тут мы добавим n-ое кол-во добавок
    let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
    h4 add.topping(Hamburger.TOPPING_SPICE);
    h4 add.topping(Hamburger.TOPPING_SPICE);
    HamburgerException: duplicate topping "TOPPING_SPICE";
    */
} catch (e) {
    console.log(e);
}