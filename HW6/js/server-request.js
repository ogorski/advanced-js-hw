class serverRequest {
    constructor(url) {
        this._url = url;
    }

    async send(type, at, value = false) {
        if (value == false) {
            const response = await fetch(`${this._url}/${at}`, {
                method: type,
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(response => response.json());
        } else {
            const response_2 = await fetch(`${this._url}/${at}`, {
                method: type,
                body: JSON.stringify(value),
                headers: {
                    'Content-type': 'application/json'
                }
            });
            return await response_2.json();
        }
    }
}