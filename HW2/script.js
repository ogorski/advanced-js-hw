class Hamburger {
    constructor(size, stuffing) { // добавляем размер и начинку
        if (arguments.length !== 2) {
            throw newHamburgerException('');
        } else if (!Hamburger.TYPE_SIZE.includes(size)) {
            throw newHamburgerException('');
        } else if (!Hamburger.TYPE_STUFFING.includes(stuffing)) {
            throw newHamburgerException('');
        }

        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }
    // задаем характеристики параметрам, которые мы будем выдавать гамбургеру

    static SIZE_SMALL = {size: "small", price: "10", calories: "45" }
    static SIZE_LARGE = {size: "large", price: "25", calories: "100" }
    static STUFFING_CHEESE = {stuffing: "cheese", price: "5", calories: "2"}
    static STUFFING_SALAD = {stuffing: "salad", price: "10", calories: "50"}
    static STUFFING_POTATO = {stuffing: "potato", price: "25", calories: "60"}
    static TOPPING_MAYO = {topping: "mayo", price: "5", calories: "40"}
    static TOPPING_SPICE = {topping: "spice", price: "10", calories: "10"}

    static TYPE_SIZE = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
    static TYPE_STUFFING = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD];
    static TYPE_TOPPING = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];

    addTopping(addedTopping) { // добавляем добавку
        if (arguments.length !== 1) {
            throw newHamburgerException('');
        }
        if (!Hamburger.TYPE_TOPPING.includes(topping)) {
            throw newHamburgerException('');
        }
        this.topping.forEach(item => {
            if (item === itemTopping) {
                throw newHamburgerException('');
            }
        })
        this._topping.push(addedTopping);
    }

    removeTopping(removedTopping) { // убираем добавку
        if (arguments.length !== 1) {
            throw newHamburgerException('');
        } else if (!Hamburger.TYPE_TOPPING.includes(topping)) {
            throw newHamburgerException('');
        } else if (this.topping.length === 0) {
            throw newHamburgerException('');
        }

        if (this.topping.indexOf(removedTopping) !== -1) {
            this._topping.splice(this._topping.indexOf(removedTopping), 1);
        } else {
            throw newHamburgerException('');
        }
    }

    get Toppings() { // узнаем добавку к гамбургеру
        return this._topping.map(el => el.topping);
    }

    get Size() { // узнаем размер гамбургера
        return this._size.size;
    }

    get Stuffing() { // узнаем начинку для гамбургера
        return this._stuffing.stuffing;
    }

    calculatePrice() { // рассчитываем цену
        let priceTopping = this._topping.reduce((sum, current) => {
            return sum + current.calories;
        }, 0);
        return this._size.price + this._stuffing.price + priceTopping;
    }

    calculateCalories() { // рассчитываем калории
        let caloriesTopping = this._topping.reduce((sum, current) => {
            return sum + current.calories;
        }, 0);
        return this._size.calories + this._stuffing.calories + caloriesTopping;
    }
}

class HamburgerException extends error() {
    constructor(message) {
        super();
        this.name = "Hamburger Exception";
        this.message = message;
    }
}

try {
    // берем маленький гамбургер с салатом
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
    // добавляем начинку из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    //
    console.log("This topping has been added: ${hamburger.Stuffing}");
    // рассчитываем кол-во калорий в данном гамбургере
    console.log("Calories: %f", hamburger.calculateCalories());
    // рассчитываем цены данного гамбургера
    console.log("Price: %f", hamburger.calculatePrice());
    // добавляем начинку в виде специй
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // рассчитываем цену гамбургера с соусом
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // рассчитываем большой ли гамбургер
    console.log("Is this hamburger big: %f", hamburger.Size() === Hamburger.SIZE_LARGE); // -> false
    // убираем у гамбургера начинку из майонеза
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
    console.log("Have %d toppings", hamburger.Toppings().length); // 1
} catch (e) {
    console.log(e);
}

try {
    /* мы не передали здесь обязательные значения
    let h2 = new Hamburger(); => HamburgerException: no size given 

    передаем заведомо неправильные значения
    let h3 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_MAYO);
    => HamburgerException: invalid size: "TOPPING_SAUCE"

    тут мы добавим n-ое кол-во добавок
    let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
    h4 add.topping(Hamburger.TOPPING_SPICE);
    h4 add.topping(Hamburger.TOPPING_SPICE);
    HamburgerException: duplicate topping "TOPPING_SPICE";
    */
} catch (e) {
    console.log(e);
}