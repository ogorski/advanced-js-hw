class trelloCard {
    static newElement() { // creating Trello Card element
        const divOne = document.createElement('div');
        divOne.setAttribute('class', 'createColumn');
        divOne.setAttribute('id', 'idCreateColumn');
        document.body.appendChild(divOne);

        const divTwoTxt = document.createElement('span'); // creating span
        divTwoTxt.setAttribute('class', 'createColumnText');
        divTwoTxt.setAttribute('id', 'idCreateColumnText');
        divTwoTxt.textContent = 'Add new column here';
        divOne.append(divTwoTxt);
        divTwoTxt.addEventListener('click', () => trelloCard.newElementExpanded());
    }

    static newElementExpanded() { // creating New Expanded Element
        const div = document.getElementById('idCreateColumn');
        const text = document.getElementById('idCreateColumnText');
        const firstInput = document.createElement('input');
        firstInput.setAttribute('class', 'createColumnInput');
        firstInput.setAttribute('id', 'idCreateColumnInput');
        firstInput.setAttribute('type', 'text');
        firstInput.setAttribute('value', 'Enter list name');
        firstInput.addEventListener('click', function() {
            firstInput.setAttribute('value', '');
        });

        div.prepend(firstInput);

        const submitBtn = document.createElement('button'); // creating button which will create columns
        submitBtn.setAttribute('class', 'createColumnButton');
        submitBtn.setAttribute('id', 'idCreateColumnButton');
        submitBtn.textContent = "Please add list";
        div.append(submitBtn);
        text.textContent = '';
        submitBtn.addEventListener('click', () => trelloCard.createCard());
        submitBtn.onclick = function() {
            firstInput.value = '';
        }
    }

    static createCard() {
        const valueCardInput = document.getElementById('idCreateColumnInput').value; // creating card element
        if (valueCardInput === '' || valueCardInput === 'Enter list name') return;
        const containerCard = document.createElement('div');
        containerCard.setAttribute('class', 'primaryDiv');
        containerCard.setAttribute('id', 'idPrimaryDiv');
        document.body.append(containerCard);
        
        const textContent = document.querySelector('.createColumnInput').value; // creating header and input for cards
        const header = document.createElement('div');
        header.setAttribute('class', 'primaryHeader');
        header.textContent = textContent;
        containerCard.append(header);

        const containerNotes = document.createElement('div'); // 
        containerNotes.setAttribute('class', 'divContainer');
        containerNotes.setAttribute('id', 'list');
        containerCard.append(containerNotes);
        containerNotes.addEventListener('dragover', (e) => trelloCard.dragOverTarget(e));
        containerNotes.addEventListener('drop', (e) => trelloCard.dropTarget(e));

        const sortCards = document.createElement('div'); // create button which will sort our cards
        sortCards.setAttribute('class', 'sortCards');
        sortCards.textContent = '...';
        containerCard.append(sortCards);
        sortCards.addEventListener('click', (e) => trelloCard.sortCardsBtn(containerNotes));

        const inputCards = document.createElement('input'); // creating input for our cards
        inputCards.setAttribute('class', 'inputCards');
        inputCards.setAttribute('id', 'createInput');
        inputCards.setAttribute('type', 'text');
        inputCards.setAttribute('value', 'Please name heading for this card');
        containerCard.append(inputCards);
        inputCards.addEventListener('click', function() {
            inputCards.setAttribute('value', '');
        });

        const buttonCard = document.createElement('div'); // creating button element
        buttonCard.setAttribute('class', 'btnCardInside');
        buttonCard.setAttribute('id', 'idBtnCardInside');
        buttonCard.textContent = 'Please add card';
        containerCard.append(buttonCard);
        buttonCard.addEventListener('click', () => trelloCard.createCardNote(inputCards, containerNotes));
        buttonCard.onclick = function() {
            inputCards.value = '';
        }
    }

    static createCardNote(inputCards, containerNotes) { // creating card notes which will appear
        if (inputCards.value === '' || inputCards.value === 'Please name heading for this card') return;
        let cardCover = document.createElement('div');
        cardCover.setAttribute('class', 'itemText');
        cardCover.setAttribute('type', 'text');
        cardCover.setAttribute('id', `note-item-${inputCards.value}`);
        cardCover.setAttribute('draggable', 'true');
        cardCover.textContent = inputCards.value;
        containerNotes.append(cardCover);
        cardCover.addEventListener('dragstart', (e) => trelloCard.startDragTarget(e));
    }

    static sortCardsBtn(containerNotes) { // making card sorting
        let toSortCards = containerNotes.children;
        let newCardList = [];

        for (let z = 0; z < toSortCards.length; z++) {
            newCardList.push(toSortCards[z].textContent);
        }

        let cardResults = newCardList.sort(function compare(a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            if (a === b) return 0;
            return newCardList;
        });
        
        containerNotes.innerHTML = "";
        for (let i = 0; i < cardResults.length; i++) {
            let cardCover = document.createElement('div');
            cardCover.setAttribute('class', 'itemText');
            cardCover.setAttribute('type', 'text');
            cardCover.setAttribute('id', `note-item-${cardResults[i]}`);
            cardCover.setAttribute('draggable', 'true');
            cardCover.textContent = cardResults[i];
            containerNotes.append(cardCover);
            cardCover.addEventListener('dragstart', (e) => trelloCard.startDragTarget(e));
        } 
    }

    static dragOverTarget(e) {
        e.preventDefault();
    }

    static dropTarget(e) {
        e.preventDefault();
        const dragSrc = document.getElementById(`note-item-${e.dataTransfer.getData('srcId')}`);
        src.id = e.target.id;
        e.target.id = `note-item-${e.dataTransfer.getData('srcId')}`;
        src.innerHTML = e.target.innerHTML;
        e.target.innerHTML = e.dataTransfer.getData('srcId');
    }

    static startDragTarget(e) {
        e.preventDefault();
        e.dataTransfer.setData('srcId', e.target.innerHTML);
    }
}

trelloCard.newElement();